package com.example.Model;

public class UserV2 extends User {
    public UserV2(){ };
    public UserV2(int _id, String _name, String _lang) {
        super(_id, _name);
        this.lang = _lang;
        setPostURL();
    }

    private String lang;
    private String postURL;
    public String getLang() {
        return lang;
    }
    public void setLang(String lang) {this.lang = lang;}

    public String getPostURL(){
        return postURL;
    }
    public void setPostURL(){
        this.postURL = Constants.BASE_URL + "users/" + getId() +"/posts";
    }

}