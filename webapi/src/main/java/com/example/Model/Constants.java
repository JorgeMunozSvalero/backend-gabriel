package com.example.Model;

public class Constants {

	public static final String BASE_URL = "http://localhost:9090/";
    public static final String PAGED_USERSV2 = "/v2/users/paged/";
    public static final int PAGE_SIZE = 4;

}
