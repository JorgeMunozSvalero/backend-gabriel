package com.example.Model;

import java.util.ArrayList;

public class PagedResults<T> {
    private int pageNumber;
    private ArrayList<T> results;
    private String prevLink;
    private String nextLink;

    public PagedResults() {
    }

    public PagedResults(ArrayList<T> list, String baseURL, int pageNumber) {
        this.results = new ArrayList<>();
        this.pageNumber = pageNumber;
        int pageLink = pageNumber;
        if (list != null) {
            int startIndex = (pageNumber - 1) * Constants.PAGE_SIZE;
            int endIndex = startIndex + Constants.PAGE_SIZE;
            if (endIndex > list.size()) {
                endIndex = list.size();
            }
            for (int i = startIndex; i < endIndex; i++) {

                this.results.add(list.get(i));

            }
        }
        if (pageLink == 1) {
            this.prevLink = "No hay más páginas";
            this.nextLink = Constants.BASE_URL + Constants.PAGED_USERSV2 + pageLink++;
        } else {
            this.prevLink = Constants.BASE_URL + Constants.PAGED_USERSV2 + pageLink--;
            this.nextLink = Constants.BASE_URL + Constants.PAGED_USERSV2 + pageLink++;
        }

    }

    public ArrayList<T> getResults() {
        return results;
    }

    public void setResults(ArrayList<T> results) {
        this.results = results;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getPrevLink() {
        return prevLink;
    }

    public void setPrevLink(String prevLink) {
        this.prevLink = prevLink;
    }

    public String getNextLink() {
        return nextLink;
    }

    public void setNextLink(String nextLink) {
        this.nextLink = nextLink;
    }

}
