package com.example.API;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.Model.Constants;
import com.example.Model.PagedResults;
import com.example.Model.User;
import com.example.Model.UserV2;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsersController {

    // private static ArrayList<User> users = new ArrayList<User>(){
    // {
    // add(new User(1, "Jorge"));
    // add(new User(2, "Dani"));
    // add(new User(3, "Alex"));
    // add(new User(4, "Jaime"));
    // add(new User(5, "Gabi"));
    // };

    // };

    public static ArrayList<UserV2> usersListV2 = new ArrayList<UserV2>() {
        {
            add(new UserV2(1, "Jorge", "en-US"));
            add(new UserV2(2, "Dani", "en-UK"));
            add(new UserV2(3, "Alex", "es-ES"));
            add(new UserV2(4, "Jaime", "es-CH"));
            add(new UserV2(5, "Gabi", "it-IT"));
            add(new UserV2(6, "Dabi", "it-IT"));
            add(new UserV2(7, "Porto", "it-IT"));
        };
    };

    private UserV2 searchUser(int id) {
        
        for (UserV2 u : usersListV2) {
            if (u.getId() == id) {
               
                return u;
            }
        }
        throw new ElementNotFoundException();
    }

    //404
    @ResponseStatus(value=HttpStatus.NOT_FOUND, reason ="Nsssso element found")
    public class ElementNotFoundException extends RuntimeException{

    }

    private static ArrayList<User> convertListUsersV2toUsersV1(ArrayList<UserV2> usersV2list) {
        ArrayList<User> usersv1 = new ArrayList<>();
        for (User u : usersListV2) {
            usersv1.add(new User(u.getId(), u.getName()));
        }
        return usersv1;
    }

    private static User convertUserV2toUserV1(UserV2 userV2) {
        User userv1 = new User();
        userv1.setId(userV2.getId());
        userv1.setName(userV2.getName());

        return userv1;
    }

    @GetMapping("/v1/users")
    public ArrayList<User> UsersV1() {
        return convertListUsersV2toUsersV1(usersListV2);
    }

    

    @GetMapping("/v2/users")
    public ArrayList<UserV2> UsersV2() {
        return usersListV2;
    }

    // Probar ERROR 404. 
    @GetMapping("/v1/users/{id}")
    public User UserV1ById(@PathVariable("id") int id, 
                            HttpServletRequest request,
                            HttpServletResponse response) {
    
        if (searchUser(id) == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        User userv1 = new User();
        userv1 = convertUserV2toUserV1(searchUser(id));
       return userv1;
    }

    // @GetMapping("/v1/users/{id}")
    // public ResponseEntity<User> UserById(@PathVariable("id") int id) {
    //     try {
    //         return new ResponseEntity<>(UserV1ById(id), HttpStatus.OK);
    //     } catch (Exception ex) {
    //         return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    //     }
    // }

    @GetMapping("/v2/users/{id}")
    public UserV2 UserV2ById(@PathVariable("id") int id) {
            return searchUser(id);
    }

    @PostMapping("/v1/users")
    public User Addv1(@RequestBody User newUser) {
        if (usersListV2.contains(newUser)) {
            return null;
        } else {
            UserV2 userWrapped = new UserV2();
            userWrapped.setId(newUser.getId());
            userWrapped.setName(newUser.getName());
            usersListV2.add(userWrapped);
            return newUser;
        }
    }

    @PostMapping("/v2/users")
    public UserV2 Addv2(@RequestBody UserV2 newUser) {
        if (null == searchUser(newUser.getId())) {
            return null;
        } else {
            usersListV2.add(newUser);
            return newUser;
        }
    }

    @PutMapping("/v1/users/{id}")
    public UserV2 Update(@RequestBody UserV2 updateUser, @PathVariable("id") int id) {

        if (searchUser(id) != null) {
            searchUser(id).setName(updateUser.getName());
            return searchUser(id);
        }
        return null;
    }

    @PutMapping("/v2/users/{id}")
    public UserV2 UpdateV2(@RequestBody UserV2 updateUser, @PathVariable("id") int id) {

        if (searchUser(id) != null) {
            searchUser(id).setName(updateUser.getName());
            return searchUser(id);
        }
        return null;
    }

    @DeleteMapping("/v2/users/{id}")
    public void Delete(@PathVariable("id") int id) {
        usersListV2.remove(searchUser(id));
    }

    @GetMapping("/v2/users/paged/{pageNumber}")
    public PagedResults<UserV2> UsersV2(@PathVariable("pageNumber") int pageNumber) {

        return new PagedResults<>(usersListV2, Constants.BASE_URL, pageNumber);

    }

}