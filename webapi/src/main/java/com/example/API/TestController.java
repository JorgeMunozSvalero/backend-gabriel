package com.example.API;

import java.util.Locale;
import java.util.ResourceBundle;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TestController {
    @GetMapping("/test2html")
    public String Tests(Locale locale) {

        var messages = ResourceBundle.getBundle("i18n\\messages", locale);

        return messages.getString("Main.Hello");
    }

    @GetMapping("/testHtml")
    public String Test() {
        return "test";
    }

    @GetMapping("/testHtml2")
    public String Test2() {
        return "test2";
    }

    @GetMapping("/testHtml3")
    public String Test3() {
        return "test3";
    }

}
