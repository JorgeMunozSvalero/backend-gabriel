package com.example.API;


import java.util.Locale;
import java.util.ResourceBundle;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class UsersViewController {

    @GetMapping("/test")
    public String Test(Locale locale){
        var messages = ResourceBundle.getBundle("i18n\\messages", locale);
        return messages.getString("hello");
    }
    @GetMapping("/users")
    public ModelAndView UsersV2() {
        ModelAndView mv = new ModelAndView("listUsers");
        mv.addObject("usersListV2", UsersController.usersListV2);
        return mv;
    }
}