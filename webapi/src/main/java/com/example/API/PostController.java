package com.example.API;

import java.util.ArrayList;

import com.example.Model.Post;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PostController {

    @GetMapping("/posts")
    public ArrayList<Post> Hello() {

        Post post = new Post(1, 1, "First post");
        Post post1 = new Post(2, 1, "Second post");
        Post post2 = new Post(3, 1, "Third post");

        ArrayList<Post> posts = new ArrayList<>();
        posts.add(post);
        posts.add(post1);
        posts.add(post2);

        return posts;
    }

    @GetMapping("/users/{userid}/posts")
    public ArrayList<Post> id(@PathVariable("userid") int userid) {

        Post post = new Post(1, 1, "First post");
        Post post1 = new Post(2, 1, "Second post");
        Post post2 = new Post(3, 1, "Third post");
        Post post1_2 = new Post(1, 2, "prime post user 2");
        Post post2_2 = new Post(2, 2, "seg post user 2");

        ArrayList<Post> posts = new ArrayList<>();
        posts.add(post);
        posts.add(post1);
        posts.add(post2);
        posts.add(post1_2);
        posts.add(post2_2);

        ArrayList<Post> postuserId = new ArrayList<>();
        for (Post i : posts) {
            if (i.getUserId() == userid) {

                postuserId.add(i);

            }
        }
        return postuserId;
    }

}