package sv.jorge.API;

import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import sv.jorge.Model.Film;

@RestController
public class FilmsController {

    // ==========================================================
    // ==========================================================
    // ==========================================================
    // ======================= TO DO ============================
    // ==========================================================
    // ==========================================================
    // ==========================================================

    // Añadir excepcion en funciones con return null para mostrar algo controlado
    // por pantalla

    private static ArrayList<Film> filmList = new ArrayList<Film>() {
        {
            add(new Film(1, "Lagrimas de sangre", 2009, "http://loremflickr.com/320/240/film"));
            add(new Film(2, "Lo imposible", 2015, "http://loremflickr.com/320/240/film"));
            add(new Film(2, "Lo mejor", 2016, "http://loremflickr.com/320/240/film"));
            add(new Film(3, "007. Imparable", 2015, "http://loremflickr.com/320/240/film"));
            add(new Film(4, "Chuck Norris revenge", 1990, "http://loremflickr.com/320/240/film"));
            add(new Film(5, "Chuck Norris revenge 2", 2000, "http://loremflickr.com/320/240/film"));
            add(new Film(6, "Homeland Security", 2002, "http://loremflickr.com/320/240/film"));
        }
    };

    private Film getFilmById(Integer id) {
        for (Film film : filmList) {
            if (film.getId() == (int) id) {
                return film;
            }
        }
        // throw new ElementNotFoundException();
        return null;
    }

    private ArrayList<Film> getFilmByYear(ArrayList<Film> list, Integer yr) {

        boolean found = false;
        ArrayList<Film> films = new ArrayList<>();

        if (yr == null) {
            return list;
        }
        for (Film film : list) {
            if (film.getYear() == (int) yr) {
                films.add(film);
                found = true;
            }
        }
        if (found)
            return films;
        return null;
    }

    private ArrayList<Film> getFilmByTitle(String title) {
        boolean checked = false;
        if (title == null) {
            return filmList;
        }
        ArrayList<Film> filmsByTitle = new ArrayList<>();
        for (Film film : filmList) {
            if (film.getTitle().contains(title)) {
                filmsByTitle.add(film);
                checked = true;
            }
        }
        if (checked)
            return filmsByTitle;
        return null;
    }

    @GetMapping("/films")
    public ArrayList<Film> getFilms(@RequestParam(name = "title", required = false) String title,
            @RequestParam(name = "year", required = false) Integer year) {

        if ((title != null) && (year != null)) {
            ArrayList<Film> filmsFilteredTitle = new ArrayList<>();
            filmsFilteredTitle = getFilmByTitle(title);
            return getFilmByYear(filmsFilteredTitle, year);
        }
        if ((title != null) || (year != null)) {
            ArrayList<Film> filmsFilteredTitle = new ArrayList<>();
            filmsFilteredTitle = getFilmByTitle(title);
            return getFilmByYear(filmsFilteredTitle, year);
        }
        return filmList;

    }

    @PostMapping("/film")
    public Film AddFilm(@RequestBody Film newFilm) {
        if (getFilmById(newFilm.getId()) != null) {
            return null;
        } else {
            Film tmpFilm = new Film();
            tmpFilm.setId(newFilm.getId());
            tmpFilm.setTitle(newFilm.getTitle());
            tmpFilm.setYear(newFilm.getYear());
            tmpFilm.setPoster(newFilm.getPoster());
            filmList.add(tmpFilm);
            return newFilm;
        }
    }

    @PutMapping("/film")
    public Film UpdateFilm(@RequestBody Film newFilm) {
        if (getFilmById(newFilm.getId()) == null) {
            return null;
        } else {

            /*
             * En esta situacion se puede, pero no con bbdd autoincremental (sin otras
             * herramientas).
             * 
             * filmList.remove(newFilm.getId()); filmList.add(newFilm);
             * 
             */

            if (newFilm.getYear() != 0)
                getFilmById(newFilm.getId()).setTitle(newFilm.getTitle());
            if (newFilm.getTitle() != null)
                getFilmById(newFilm.getId()).setTitle(newFilm.getTitle());
            if (newFilm.getYear() != 0)
                getFilmById(newFilm.getId()).setYear(newFilm.getYear());
            if (newFilm.getPoster() != null)
                getFilmById(newFilm.getId()).setPoster(newFilm.getPoster());
            return getFilmById(newFilm.getId());
        }
    }

    @DeleteMapping("/film/{id}")
    public void Delete(@PathVariable("id") int id) {
        if (getFilmById(id) != null)
            filmList.remove(getFilmById(id));
    }

    

}